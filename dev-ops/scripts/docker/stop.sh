#!/usr/bin/env bash
#DESCRIPTION: Stops containers

docker-compose stop
