#!/usr/bin/env bash
#DESCRIPTION: starts docker dev environment  USAGE: docker:start --asp=asp0000 --host=dev.shopware.local --ip=127.0.0.1

cp dev-ops/customers/__ASP__/docker-compose.override.yml .

./dev-ops/hosts/hostscreator.sh __IP__ __HOST__

docker-compose build
docker-compose up -d rabbitmq
docker-compose up -d mysql

sleep 10

docker-compose up -d
