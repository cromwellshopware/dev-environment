# CBS Shopware Integration Dev Environment

## Requirements
- Docker
- PHP 7

## Usage

### init
After cloning repository you have to get latest code for every integration component. You can do this by running:

    ./psh.phar init

This will clone all required repositories, install dependencies and create .env files.

### docker:start

To start the dev environment run following:

    ./psh.phar docker:start --asp=asp0000 --host=dev.shopware.local --ip=127.0.0.1

docker:start takes three parameters:

* *asp* - selects integration to run. Available integrations are: asp0000, asp0028, asp0102
* *host* - this is your local shopware installation hostname.
* *ip* - this is ip address of your local shopware installation

*host* and *ip* are used to create hosts file on docker containers so specified ip needs to be reachable from the container

### docker:stop

To stop your dev environment run following:

    ./psh.phar docker:stop

It will stop all docker containers without removing any data persistent data (mysql and rabbitmq containers)

### docker:down

To stop and remove all containers data and network run:

	./psh.phar docker:down

