#!/usr/bin/env bash
#DESCRIPTION: initialization of your dev environment

git clone git@bitbucket.org:cromwellshopware/client.git client/client/
git clone git@bitbucket.org:hughesshopware/integration-client-htv.git client/integration-client-htv/
git clone git@bitbucket.org:cromwellshopware/integration-client-erp.git client/integration-client-erp/
git clone git@bitbucket.org:cromwellshopware/integration-client-or.git client/integration-client-or/

git clone git@bitbucket.org:cromwellshopware/consumer.git integrate/consumer/
git clone git@bitbucket.org:cromwellshopware/integration-consumer-erp.git integrate/integration-consumer-erp/
git clone git@bitbucket.org:cromwellshopware/integration-consumer-or.git integrate/integration-consumer-or/
git clone git@bitbucket.org:cromwellshopware/integration-consumer-sw.git integrate/integration-consumer-sw/
git clone git@bitbucket.org:hughesshopware/integration-consumer-swhtv.git integrate/integration-consumer-swhtv/

git clone git@bitbucket.org:cromwellshopware/scheduler.git integrate/scheduler/

git clone git@bitbucket.org:cromwellshopware/config.git integrate/config/

git clone git@bitbucket.org:cromwellshopware/token-producer.git integrate/token-producer/

git clone git@bitbucket.org:cromwellshopware/cbs.git common/cbs/

cd client/client && npm install
cd client/integration-client-htv && npm install
I: cd client/integration-client-erp && npm install
cd client/integration-client-or && npm install

cd integrate/consumer && npm install
cd integrate/integration-consumer-erp && npm install
cp dev-ops/templates/docker.env integrate/integration-consumer-erp/.env
cd integrate/integration-consumer-or && npm install
cp dev-ops/templates/docker.env integrate/integration-consumer-or/.env
cd integrate/integration-consumer-sw && npm install
cp dev-ops/templates/docker.env integrate/integration-consumer-sw/.env
cd integrate/integration-consumer-swhtv && npm install
cp dev-ops/templates/docker.env integrate/integration-consumer-swhtv/.env

cd integrate/scheduler && npm install
cp dev-ops/templates/docker.env integrate/scheduler/.env

cd integrate/config && npm install
cp dev-ops/templates/docker.env integrate/config/.env

cd integrate/token-producer && npm install
cp dev-ops/templates/docker.env integrate/token-producer/.env

cd common/cbs && npm install
