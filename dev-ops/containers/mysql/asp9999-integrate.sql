# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.38)
# Database: asp9999-integrate
# Generation Time: 2018-01-12 12:45:48 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table int_error
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int_error`;

CREATE TABLE `int_error` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `consumer` varchar(128) DEFAULT NULL,
  `error` text,
  `message` longtext,
  `resolved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table int_channel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int_channel`;

CREATE TABLE `int_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customerId` int(11) unsigned NOT NULL,
  `name` varchar(11) DEFAULT NULL,
  `environment` enum('live','test') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customerId` (`customerId`),
  CONSTRAINT `int_channel_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `int_customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int_channel` WRITE;
/*!40000 ALTER TABLE `int_channel` DISABLE KEYS */;

INSERT INTO `int_channel` (`id`, `customerId`, `name`, `environment`)
VALUES
	(7,4,'erp','test'),
	(10,7,'or','test'),
	(28,25,'htv','test');

/*!40000 ALTER TABLE `int_channel` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int_consumer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int_consumer`;

CREATE TABLE `int_consumer` (
  `name` varchar(128) NOT NULL DEFAULT '',
  `channelId` int(11) unsigned DEFAULT NULL,
  `endpointId` int(11) unsigned DEFAULT NULL,
  `queue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int_consumer` WRITE;
/*!40000 ALTER TABLE `int_consumer` DISABLE KEYS */;

INSERT INTO `int_consumer` (`name`, `channelId`, `endpointId`, `queue`)
VALUES
	('asp0000.or.dev.consumer.or',10,4,'asp0000.or.dev.queue.or'),
	('asp0000.or.dev.consumer.sw',10,1,'asp0000.or.dev.queue.sw'),
	('asp0028.or.dev.consumer.or',28,64,'asp0028.or.dev.queue.or'),
	('asp0028.or.dev.consumer.sw',28,61,'asp0028.or.dev.queue.sw'),
	('asp0102.erp.dev.consumer.erp',7,46,'asp0102.erp.dev.queue.erp');

/*!40000 ALTER TABLE `int_consumer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int_customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int_customer`;

CREATE TABLE `int_customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customerRef` varchar(16) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int_customer` WRITE;
/*!40000 ALTER TABLE `int_customer` DISABLE KEYS */;

INSERT INTO `int_customer` (`id`, `customerRef`, `name`)
VALUES
	(4,'asp0102','Stellisons Electrical'),
	(7,'asp0012','Hylands'),
	(25,'asp0028','Hughes');

/*!40000 ALTER TABLE `int_customer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int_endpoint
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int_endpoint`;

CREATE TABLE `int_endpoint` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `tokenBucketQueue` varchar(128) DEFAULT '',
  `rpm` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int_endpoint` WRITE;
/*!40000 ALTER TABLE `int_endpoint` DISABLE KEYS */;

INSERT INTO `int_endpoint` (`id`, `url`, `tokenBucketQueue`, `rpm`)
VALUES
	(1,'https://client:14028','dev.tb',10),
	(4,'http://dev.hylandselectrical.co.uk','dev.tb',10),
	(43,'https://asp0102.integrate.cbsquared.co.uk:14800','dev.tb',10),
	(46,'http://dev.stellisonselectrical.co.uk','dev.tb',10),
	(61,'https://client:14028','dev.tb',10),
	(64,'https://dev.hughes.co.uk','dev.tb',10);

/*!40000 ALTER TABLE `int_endpoint` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int_endpoint_auth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int_endpoint_auth`;

CREATE TABLE `int_endpoint_auth` (
  `endpointId` int(11) unsigned NOT NULL,
  `type` enum('basic','digest') NOT NULL DEFAULT 'basic',
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int_endpoint_auth` WRITE;
/*!40000 ALTER TABLE `int_endpoint_auth` DISABLE KEYS */;

INSERT INTO `int_endpoint_auth` (`endpointId`, `type`, `username`, `password`)
VALUES
	(4,'digest','api.access','sdMMUduPZ5B4iGEF0xyF0N0d8VKC8ykTz2rUgkQd'),
	(46,'digest','api','v2lgV5rvWDGB9nD81MJFkFmRwSOtZesAVxOvnBlH'),
	(64,'digest','api.tester','q2oxG0GPAzd3AZAZQTET090WzJBlfx2XUtNcQJR5');

/*!40000 ALTER TABLE `int_endpoint_auth` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table int_job
# ------------------------------------------------------------

DROP TABLE IF EXISTS `int_job`;

CREATE TABLE `int_job` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channelId` int(11) unsigned DEFAULT NULL,
  `endpointId` int(11) unsigned DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `desc` varchar(32) DEFAULT NULL,
  `queue` varchar(255) DEFAULT '',
  `exchange` varchar(255) DEFAULT NULL,
  `lastRun` datetime DEFAULT '0000-00-00 00:00:00',
  `lastRunHost` varchar(32) DEFAULT NULL,
  `interval` int(11) unsigned NOT NULL,
  `monitoringIntervalMultiple` int(11) DEFAULT '2',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `channelId` (`channelId`),
  CONSTRAINT `int_job_ibfk_1` FOREIGN KEY (`channelId`) REFERENCES `int_channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `int_job` WRITE;
/*!40000 ALTER TABLE `int_job` DISABLE KEYS */;

INSERT INTO `int_job` (`id`, `channelId`, `endpointId`, `path`, `desc`, `queue`, `exchange`, `lastRun`, `lastRunHost`, `interval`, `monitoringIntervalMultiple`, `locked`, `active`)
VALUES
	(1,10,1,'/customergroups','Fetch Affinities','asp0000.or.dev.queue.or','','2018-01-05 15:47:13','cbs-in2',60,2,0,1),
	(4,10,1,'/articles','Fetch Products','asp0000.or.dev.queue.or','','2018-01-05 15:47:13','cbs-in2',60,2,0,1),
	(7,10,1,'/manufacturers','Fetch Manufacturers','asp0000.or.dev.queue.or','','2018-01-05 15:46:59','cbs-in2',60,2,0,1),
	(10,10,1,'/orders','Fetch Order Updates','asp0000.or.dev.queue.or','','2018-01-05 15:46:59','cbs-in2',60,2,0,1),
	(13,10,4,'/api/CBSOrders','Fetch Orders','asp0000.or.dev.queue.sw','','2018-01-05 15:47:12','cbs-in2',120,2,0,1),
	(91,28,61,'/advertcodes','Fetch Advert Codes','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:46','b4a4d1302514',10,2,0,1),
	(94,28,61,'/articles','Fetch Products','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:44','b4a4d1302514',10,2,0,1),
	(97,28,61,'/categories','Fetch Categories','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:47','b4a4d1302514',10,2,0,1),
	(100,28,61,'/chargecodes','Fetch Charge Codes','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:43','b4a4d1302514',10,2,0,1),
	(103,28,61,'/clearance','Fetch Clearance','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:47','b4a4d1302514',10,2,0,1),
	(106,28,61,'/customergroups','Fetch Customer Groups','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:46','b4a4d1302514',10,2,0,1),
	(109,28,61,'/guarantees','Fetch Guarantees','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:49','b4a4d1302514',10,2,0,1),
	(112,28,61,'/locations','Fetch Locations','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:49','b4a4d1302514',10,2,0,1),
	(115,28,61,'/manufacturers','Fetch Manufacturers','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:49','b4a4d1302514',10,2,0,1),
	(124,28,61,'/propertygroups','Fetch Property Groups','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:50','b4a4d1302514',10,2,0,1),
	(127,28,61,'/propertyoptions','Fetch Property Options','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:40','b4a4d1302514',10,2,0,1),
	(130,28,61,'/staff','Fetch Staff','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:43','b4a4d1302514',10,2,0,1),
	(133,28,61,'/stafftypes','Fetch Staff Types','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:43','b4a4d1302514',10,2,0,1),
	(136,28,61,'/stockavailability','Fetch Stock Availability','asp0028.or.dev.queue.or',NULL,'2018-01-12 12:45:44','b4a4d1302514',10,2,0,1),
	(139,28,64,'/api/htvorders','Fetch Orders','asp0028.or.dev.queue.sw',NULL,'2018-01-12 12:45:30','b4a4d1302514',20,2,1,1),
	(142,28,64,'/api/htvcustomers','Fetch Customer Updates','asp0028.or.dev.queue.sw',NULL,'2018-01-12 12:45:48','b4a4d1302514',20,2,0,1),
	(145,28,64,'/api/htvrentalpayment','Fetch Rental Payments','asp0028.or.dev.queue.sw',NULL,'2018-01-12 12:45:44','b4a4d1302514',20,2,0,1),
	(148,28,64,'/api/htvrentalservice','Fetch Rental Service Jobs','asp0028.or.dev.queue.sw',NULL,'2018-01-12 12:45:44','b4a4d1302514',20,2,0,1),
	(151,28,64,'/api/htvstaff','Fetch Staff Updates','asp0028.or.dev.queue.sw',NULL,NULL,NULL,20,2,0,0),
	(163,7,43,'/manufacturers','Fetch Manufacturers','asp0102.erp.dev.queue.erp',NULL,NULL,NULL,20,2,0,0),
	(166,7,43,'/products','Fetch Products','asp0102.erp.dev.queue.erp',NULL,NULL,NULL,20,2,0,0);

/*!40000 ALTER TABLE `int_job` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table SOAPQueue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `SOAPQueue`;

CREATE TABLE `SOAPQueue` (
  `primary_key` text NOT NULL,
  `soapmessage` text NOT NULL,
  `dateandtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `SOAPQueue` WRITE;
/*!40000 ALTER TABLE `SOAPQueue` DISABLE KEYS */;

INSERT INTO `SOAPQueue` (`primary_key`, `soapmessage`, `dateandtime`)
VALUES
	('HUGS14043ED','<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><m:PutSalesOrder TimeStampGMT=\"2013-03-13UTC11:11:34\" Version=\"1.0\" xmlns:m=\"http://www.cromwells.co.uk/gateway/\"><FinanceReference>22910</FinanceReference><UID>HUGS14043ED</UID><OrderId>AUTO</OrderId><OrderRef>HUGS14043ED</OrderRef><UserId>HWEB</UserId><CustomerNo>SW137F20E</CustomerNo><ShopwareFlag>Y</ShopwareFlag><AffinityDetails><AffinityId>HUGHES</AffinityId></AffinityDetails><PersonalDetails><Title>Mr</Title><Forename>Bartek</Forename><Surname>Niewinski</Surname><AddressLines><Address>Flat Kirkwood House</Address><Address> </Address><Address>Coulter</Address></AddressLines><Town>Biggar</Town><County></County><Postcode>ML12 6PP</Postcode><TelephoneWork></TelephoneWork><TelephoneHome>01353 650900</TelephoneHome><Mobile>01353 650900</Mobile><Fax></Fax><emailAddress>bniewinski@cromwells.co.uk</emailAddress><MailFlag></MailFlag><ExternalMailFlag></ExternalMailFlag><PreferredStore></PreferredStore><emailFlag></emailFlag><Password></Password></PersonalDetails><DeliveryDetails><DeliveryCompanyName></DeliveryCompanyName><DeliveryAddressLines><DeliveryAddress>Flat Kirkwood House</DeliveryAddress><DeliveryAddress> </DeliveryAddress><DeliveryAddress>Coulter</DeliveryAddress></DeliveryAddressLines><DeliveryTown>Biggar</DeliveryTown><DeliveryCounty></DeliveryCounty><DeliveryPostcode>ML12 6PP</DeliveryPostcode><DeliveryDate>TBA</DeliveryDate><DeliveryTime></DeliveryTime><DeliveryInstructions><DeliveryComments>Tel Mob: 01353 650900</DeliveryComments><DeliveryComments>Tel Work: </DeliveryComments><DeliveryComments>Tel Home: 01353 650900</DeliveryComments></DeliveryInstructions><DeliveryName>Mr Bartek Niewinski</DeliveryName></DeliveryDetails><ProductDetails><ProductId>SAM-UE55MU6500</ProductId><Quantity>1</Quantity><Price>1199.00</Price><OverrideLineAction></OverrideLineAction><Charges><ChargeCode>CC:C89</ChargeCode><ChargeAmount>0.00</ChargeAmount></Charges><VoucherCode></VoucherCode><VoucherDiscount>0.00</VoucherDiscount><StockBranch></StockBranch><DeliveryBranch></DeliveryBranch><OverrideCarrier></OverrideCarrier><OverrideDeliveryDate>TBA</OverrideDeliveryDate><WarrantyId></WarrantyId><WarrantyPrice></WarrantyPrice></ProductDetails><PaymentDetails><PaymentType>CRD</PaymentType><AuthorisationReference></AuthorisationReference><PaymentAmount>0.00</PaymentAmount><AuthorisationCode></AuthorisationCode></PaymentDetails><ReferringWebsite></ReferringWebsite><CommentLines><SaleComments></SaleComments></CommentLines><AdvertCode></AdvertCode><UseOverrideBranch></UseOverrideBranch><OverrideSaleType></OverrideSaleType><OverrideSaleOption></OverrideSaleOption><OverrideAgreementBranch></OverrideAgreementBranch><OverrideApprovalCode></OverrideApprovalCode><AgreementTasks><Task><TaskId>WEBF</TaskId></Task></AgreementTasks><FinanceCode></FinanceCode><SubsidyRate></SubsidyRate></m:PutSalesOrder></SOAP-ENV:Body></SOAP-ENV:Envelope>','2018-01-12 11:23:36');

/*!40000 ALTER TABLE `SOAPQueue` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table SOAPQueue_Archive
# ------------------------------------------------------------

DROP TABLE IF EXISTS `SOAPQueue_Archive`;

CREATE TABLE `SOAPQueue_Archive` (
  `primary_key` text NOT NULL,
  `soapmessage` text NOT NULL,
  `dateandtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `SOAPQueue_Archive` WRITE;
/*!40000 ALTER TABLE `SOAPQueue_Archive` DISABLE KEYS */;

INSERT INTO `SOAPQueue_Archive` (`primary_key`, `soapmessage`, `dateandtime`)
VALUES
	('HUGS14043ED','<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><m:PutSalesOrder TimeStampGMT=\"2013-03-13UTC11:11:34\" Version=\"1.0\" xmlns:m=\"http://www.cromwells.co.uk/gateway/\"><FinanceReference>22910</FinanceReference><UID>HUGS14043ED</UID><OrderId>AUTO</OrderId><OrderRef>HUGS14043ED</OrderRef><UserId>HWEB</UserId><CustomerNo>SW137F20E</CustomerNo><ShopwareFlag>Y</ShopwareFlag><AffinityDetails><AffinityId>HUGHES</AffinityId></AffinityDetails><PersonalDetails><Title>Mr</Title><Forename>Bartek</Forename><Surname>Niewinski</Surname><AddressLines><Address>Flat Kirkwood House</Address><Address> </Address><Address>Coulter</Address></AddressLines><Town>Biggar</Town><County></County><Postcode>ML12 6PP</Postcode><TelephoneWork></TelephoneWork><TelephoneHome>01353 650900</TelephoneHome><Mobile>01353 650900</Mobile><Fax></Fax><emailAddress>bniewinski@cromwells.co.uk</emailAddress><MailFlag></MailFlag><ExternalMailFlag></ExternalMailFlag><PreferredStore></PreferredStore><emailFlag></emailFlag><Password></Password></PersonalDetails><DeliveryDetails><DeliveryCompanyName></DeliveryCompanyName><DeliveryAddressLines><DeliveryAddress>Flat Kirkwood House</DeliveryAddress><DeliveryAddress> </DeliveryAddress><DeliveryAddress>Coulter</DeliveryAddress></DeliveryAddressLines><DeliveryTown>Biggar</DeliveryTown><DeliveryCounty></DeliveryCounty><DeliveryPostcode>ML12 6PP</DeliveryPostcode><DeliveryDate>TBA</DeliveryDate><DeliveryTime></DeliveryTime><DeliveryInstructions><DeliveryComments>Tel Mob: 01353 650900</DeliveryComments><DeliveryComments>Tel Work: </DeliveryComments><DeliveryComments>Tel Home: 01353 650900</DeliveryComments></DeliveryInstructions><DeliveryName>Mr Bartek Niewinski</DeliveryName></DeliveryDetails><ProductDetails><ProductId>SAM-UE55MU6500</ProductId><Quantity>1</Quantity><Price>1199.00</Price><OverrideLineAction></OverrideLineAction><Charges><ChargeCode>CC:C89</ChargeCode><ChargeAmount>0.00</ChargeAmount></Charges><VoucherCode></VoucherCode><VoucherDiscount>0.00</VoucherDiscount><StockBranch></StockBranch><DeliveryBranch></DeliveryBranch><OverrideCarrier></OverrideCarrier><OverrideDeliveryDate>TBA</OverrideDeliveryDate><WarrantyId></WarrantyId><WarrantyPrice></WarrantyPrice></ProductDetails><PaymentDetails><PaymentType>CRD</PaymentType><AuthorisationReference></AuthorisationReference><PaymentAmount>0.00</PaymentAmount><AuthorisationCode></AuthorisationCode></PaymentDetails><ReferringWebsite></ReferringWebsite><CommentLines><SaleComments></SaleComments></CommentLines><AdvertCode></AdvertCode><UseOverrideBranch></UseOverrideBranch><OverrideSaleType></OverrideSaleType><OverrideSaleOption></OverrideSaleOption><OverrideAgreementBranch></OverrideAgreementBranch><OverrideApprovalCode></OverrideApprovalCode><AgreementTasks><Task><TaskId>WEBF</TaskId></Task></AgreementTasks><FinanceCode></FinanceCode><SubsidyRate></SubsidyRate></m:PutSalesOrder></SOAP-ENV:Body></SOAP-ENV:Envelope>','2018-01-12 11:23:37');

/*!40000 ALTER TABLE `SOAPQueue_Archive` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
